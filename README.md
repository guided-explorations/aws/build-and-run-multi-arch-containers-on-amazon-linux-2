# Build and Run Multi-Arch Containers on Amazon Linux 2

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/aws/build-and-run-multi-arch-containers-on-amazon-linux-2)

## Overview Information

Provides the capability to create Docker images for many hardware platforms on any other platform including: linux/386, linux/arm64, linux/riscv64, linux/ppc64le, linux/s390x, linux/arm/v7, linux/arm/v6, linux/x86_64

For instance on AWS instances running amd64 or arm64 you can build images for any of the above, including using linux/arm64 to build linux/x86_64 images more cheaply and quickly.

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

- **Product Manager For This Guided Exploration**: Jefferson Jones (@jeffersonj)

- **Publish Date**: 2021-09-21

- **GitLab Version Released On**: 14.2

- **GitLab Edition Required**: 

  - For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

- **Tested On**: 
  - GitLab Docker-Executor Runner

- **References and Featured In**:

## Demonstrates These Design Requirements, Desirements and AntiPatterns

- **Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** [Cross-Platform Docker image building with Docker Buildx](https://docs.docker.com/engine/reference/commandline/buildx/)

# Docker Buildx

Docker Buildx is a CLI plugin that extends the docker command with the full support of the features provided by Moby BuildKit builder toolkit. It provides the same user experience as docker build with many new features like creating scoped builder instances and building against multiple nodes concurrently.

This Guided Exploration is only minimal changes from default behaviors.  You can look through the artifacts created and the merge history and the jobs from the merge history to see what it has done in this project.

Combining Docker Buildx with Amazon Linux 2 allows the ability to scale and provide cost control via continuous integration (CI) on commodity compute using Amazon Linux 2 and Amazon Spot instances on Amazon EC2 Auto Scaling Groups 

# Deploy a GitLab Docker BuildX Runner

For this project a plug-in runner executor scripts was created for the [GitLab HA Scaling Runner Vending Machine for AWS EC2 ASG](https://gitlab.com/guided-explorations/aws/gitlab-runner-autoscaling-aws-asg) which is [amazon-linux-2-shell-docker-buildx.sh](https://gitlab.com/guided-explorations/aws/gitlab-runner-autoscaling-aws-asg/-/raw/main/runner_configs/amazon-linux-2-shell-docker-buildx.sh)

You can follow this how to video at this specific time to learn how to deploy a runner using the above raw runner script url here: [GitLab Scaling Runner Vending Machine for AWS: HA, Scaling, Spot and Windows at 12m49s](https://youtu.be/llbSTVEeY28?list=PL05JrBw4t0KoH74CcAtP0Ze7JXjX7zT6g&t=769)

The following parameters allow your runner to use the .gitlab-ci.yml file in this project:
- `Pointer to script used to configure the runner on the instance.` (3INSTConfigurationScript) = **https://gitlab.com/guided-explorations/aws/gitlab-runner-autoscaling-aws-asg/-/raw/main/runner_configs/amazon-linux-2-shell-docker-buildx.sh**
- `An optional list of additional tags for the GitLab Runner.` (3GITLABRunnerTagList) = **docker-buildx*

To use ARM to build x86 faster and cheaper (and other arches):
- `Hardware architecture if OS Platform is Linux.` (1OSInstanceLinuxArch) = **arm64**
- `Which Amazon Prepared AMI to grab the latest of (SSM parameter path).`(9AWSRetrieveLatestAWSOSAmiIdFromSSM) = **/aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-arm64-gp2**
- `Primary instance type to be used - if available.` (5ASGInstanceType1) = **c6g.large**

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.
